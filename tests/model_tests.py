import json
import pytest
import itertools

from unittest import mock

from zerozero_pilot import BaseModel, exceptions


page = 1
page_size = 2
where = 1
fields = 2
order = 3
pk = 1


def test_model_subclass(
    set_token,
    expected_example_list_url,
    mock_response_options,
    expected_options,
):
    """Testing metadata loads lazily"""

    class Example(BaseModel):
        list_url = expected_example_list_url

    assert expected_example_list_url == Example.list_url

    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response_options
        assert expected_options == Example.metadata
        ar.assert_called_with("OPTIONS", expected_example_list_url)

    assert expected_options == Example.metadata


def test_model_with_fk(
    build_client,
    expected_exampleschild_list_url,
    expected_options_with_fk,
    mock_response_options_with_fk,
):
    """Testing related_models loads lazily"""

    class ExamplesChild(BaseModel):
        list_url = expected_exampleschild_list_url

    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response_options_with_fk
        assert (
            expected_options_with_fk["fields"]["parent"][
                "related_model"
            ].list_url
            == ExamplesChild.related_models["parent"].list_url
        )
        ar.assert_called_with("OPTIONS", expected_exampleschild_list_url)
    assert (
        expected_options_with_fk["fields"]["parent"]["related_model"].list_url
        == ExamplesChild.metadata["fields"]["parent"]["related_model"].list_url
    )
    assert ["parent"] == list(ExamplesChild.related_models.keys())
    ExamplesChild._metadata = None
    ExamplesChild.related_models  # checks it caches 2nd attempt


def test_model_init(Example, expected_options, expected_example_detail_data):
    example = Example(**expected_example_detail_data)
    assert expected_example_detail_data == example._data


@pytest.mark.parametrize(
    "list_kwargs, expected_list_request",
    [
        [
            {
                "page": page,
                "page_size": None,
                "where": where,
                "order": order,
            },
            {
                "page": page,
                "page_size": 1000,
                "query": json.dumps(
                    {
                        "where": where,
                        "order": order,
                    },
                ),
            },
        ],
        [
            {
                "page": page,
                "page_size": page_size,
                "where": where,
                "order": order,
            },
            {
                "page": page,
                "page_size": page_size,
                "query": json.dumps(
                    {
                        "where": where,
                        "order": order,
                    },
                ),
            },
        ],
        [
            {
                "page": page,
                "page_size": page_size,
                "where": where,
            },
            {
                "page": page,
                "page_size": page_size,
                "query": json.dumps(
                    {
                        "where": where,
                    },
                ),
            },
        ],
        [
            {
                "page": page,
                "page_size": page_size,
                "order": order,
            },
            {
                "page": page,
                "page_size": page_size,
                "query": json.dumps(
                    {
                        "order": order,
                    },
                ),
            },
        ],
        [
            {"page": page, "page_size": page_size},
            {
                "page": page,
                "page_size": page_size,
                "query": json.dumps({}),
            },
        ],
    ],
)
def test_model__list(
    Example,
    set_token,
    list_kwargs,
    expected_list_request,
    expected_example_list_url,
    mock_response_options,
):
    expected_example = [
        {
            "url": "http://localhost:8000/zerozero/api/test_app.Example/1/",
            "char": "test",
        },
        {
            "url": "http://localhost:8000/zerozero/api/test_app.Example/2/",
            "char": "test",
        },
    ]
    expected_return = {
        "count": 2,
        "next": None,
        "previous": None,
        "results": expected_example,
    }
    mock_response_list = pytest.mock_response_json(
        expected_return,
        200,
    )

    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response_list
        examples = Example._list(**list_kwargs)
        ar.assert_called_with(
            "GET", expected_example_list_url, params=expected_list_request
        )
    assert expected_return == examples


def test_model__list_error(
    Example,
):
    content = "some error"
    mock_response_list = pytest.mock_response_json(
        content,
        500,
    )

    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response_list
        with pytest.raises(exceptions.ZeroZeroAPIException) as excinfo:
            examples = Example._list(page=page, page_size=page_size)
            assert content == excinfo


def test_model_list(
    Example,
    set_token,
    expected_example_list_url,
    mock_response_options,
):
    page_1 = [
        {
            "url": "http://localhost:8000/zerozero/api/test_app.Example/1/",
            "char": "test",
        },
        {
            "url": "http://localhost:8000/zerozero/api/test_app.Example/2/",
            "char": "test",
        },
    ]
    page_2 = [
        {
            "url": "http://localhost:8000/zerozero/api/test_app.Example/1/",
            "char": "test",
        },
    ]

    page_1_complete = {
        "count": 3,
        "next": "http://localhost:8000/zerozero/api/test_app.Example/?page_size=2&page=2",
        "previous": None,
        "results": page_1,
    }
    page_2_complete = {
        "count": 3,
        "next": None,
        "previous": "http://localhost:8000/zerozero/api/test_app.Example/?page_size=2&page=1",
        "results": page_2,
    }
    where_value = "foo"
    order_value = "bar"

    examples = Example.list(page_size=2, where=where_value, order=order_value)

    with mock.patch.object(Example, "__init__") as init:
        init.return_value = None
        with mock.patch.object(Example, "_list") as _list:
            _list.return_value = page_1_complete
            first_page = list(itertools.islice(examples, 2))
            _list.assert_called_with(
                page=1, page_size=2, where=where_value, order=order_value
            )
        init.assert_has_calls(
            [
                mock.call(**page_1[0]),
                mock.call(**page_1[1]),
            ],
            any_order=True,
        )

    with mock.patch.object(BaseModel, "__init__") as init:
        init.return_value = None
        with mock.patch.object(Example, "_list") as _list:
            _list.return_value = page_2_complete
            second_page = list(itertools.islice(examples, 3, 5))
            _list.assert_called_with(
                page=2, page_size=2, where=where_value, order=order_value
            )
        init.assert_has_calls(
            [
                mock.call(**page_2[0]),
            ],
            any_order=True,
        )


@pytest.mark.parametrize(
    "list_kwargs, expected_list_request",
    [
        [
            {"where": where, "order": order, "fields": fields},
            {
                "query": json.dumps(
                    {
                        "where": where,
                        "order": order,
                        "fields": fields,
                    },
                ),
                "format": "csv",
            },
        ],
        [
            {
                "where": where,
                "order": order,
            },
            {
                "query": json.dumps(
                    {
                        "where": where,
                        "order": order,
                    },
                ),
                "format": "csv",
            },
        ],
        [
            {
                "where": where,
            },
            {
                "query": json.dumps(
                    {
                        "where": where,
                    },
                ),
                "format": "csv",
            },
        ],
        [
            {
                "order": order,
            },
            {
                "query": json.dumps(
                    {
                        "order": order,
                    },
                ),
                "format": "csv",
            },
        ],
        [
            {},
            {
                "query": json.dumps({}),
                "format": "csv",
            },
        ],
    ],
)
def test_model_csv(
    Example,
    set_token,
    list_kwargs,
    expected_list_request,
    expected_example_list_url,
    mock_response_options,
):
    expected_example = [
        {
            "id": 1,
            "char": "test",
        },
        {
            "id": 2,
            "char": "test",
        },
    ]

    mock_response = pytest.mock_response_csv(expected_example, 200)
    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response
        example_df = Example.csv(**list_kwargs)
        ar.assert_called_with(
            "GET", expected_example_list_url, params=expected_list_request
        )

    assert list(expected_example[0].keys()) == list(example_df.columns)

    assert (2, 2) == example_df.shape
    assert int == example_df.id.dtype
    assert (
        object == example_df.char.dtype
    )  # TODO: this is odd and maybe we should force string


def test_model_create(
    Example,
    set_token,
    expected_example_list_url,
    expected_example_detail_data,
    expected_example_detail_url,
    expected_options,
    mock_response_options,
):
    mock_response = pytest.mock_response_json(expected_example_detail_data, 201)
    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response
        mo = Example.create(**expected_example_detail_data)
    assert expected_example_detail_url == mo.url
    assert expected_example_detail_data == mo._data


def test_model_get(
    Example,
    set_token,
    expected_example_detail_url,
    expected_example_list_url,
    mock_response_options,
    expected_example_detail_data,
):
    mock_response = pytest.mock_response_json(
        expected_example_detail_data,
        200,
    )

    example = Example.get(pk)
    assert isinstance(example, BaseModel)
    assert expected_example_detail_url == example.url
    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response
        assert expected_example_detail_data == example._data
        ar.assert_called_with("GET", expected_example_detail_url)
