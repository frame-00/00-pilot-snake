import json
import pytest


from unittest import mock

from zerozero_pilot import exceptions, BaseModel


@pytest.fixture
def example(set_token, expected_example_detail_data, Example):
    _example = Example(**expected_example_detail_data)
    return _example


def test_model_object(
    example,
    expected_options,
    expected_example_detail_data,
    expected_example_detail_url,
):
    """Test if BaseModel has dataset; does not call the server"""
    assert expected_example_detail_url == example.url
    assert expected_example_detail_data == example._data
    example.char = "new value"
    assert "new value" == example._data["char"]


@pytest.fixture
def ExampleReadOnly(
    set_token,
    expected_example_list_url,
    expected_options_read_only,
):
    class _Example(BaseModel):
        list_url = expected_example_list_url

    _Example._metadata = expected_options_read_only
    return _Example


@pytest.fixture
def ExamplesChild(
    set_token,
    expected_exampleschild_list_url,
    expected_options_with_fk,
):
    class _ExamplesChild(BaseModel):
        list_url = expected_exampleschild_list_url

    _ExamplesChild._metadata = expected_options_with_fk
    return _ExamplesChild


def test_model_object_read_only(ExampleReadOnly, expected_example_detail_data):
    """Test if BaseModel has dataset does not call the server"""
    example = ExampleReadOnly(**expected_example_detail_data)
    with pytest.raises(exceptions.ZeroZeroReadOnlyException) as excinfo:
        example.char = "new value"
    assert "read only" in str(excinfo.value)


def test_model_object_with_url_only(
    set_token,
    expected_example_detail_url,
    expected_options,
    expected_example_detail_data,
    Example,
):
    """Test if BaseModel returns expected data with url passed only from GET"""
    mock_response = pytest.mock_response_json(
        expected_example_detail_data,
        200,
    )
    example = Example(url=expected_example_detail_url)

    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response
        assert expected_example_detail_data == example._data
        ar.assert_called_with("GET", expected_example_detail_url)
    assert expected_example_detail_url == example.url
    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        example._data
        ar.assert_not_called()


def test_model_object_create(
    set_token,
    Example,
    expected_example_list_url,
    expected_example_detail_data,
    expected_options,
    expected_example_detail_url,
):
    detail_data = expected_example_detail_data.copy()
    del detail_data["url"]
    mock_response = pytest.mock_response_json(expected_example_detail_data, 201)
    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response
        mo = Example.create(**detail_data)
        ar.assert_called_with(
            "POST",
            expected_example_list_url,
            data=json.dumps(detail_data),
        )
    assert expected_example_detail_url == mo.url
    assert expected_example_detail_data == mo._data


def test_model_object_create_non_201(
    set_token,
    expected_example_detail_data,
    expected_example_list_url,
    Example,
):
    detail_data = expected_example_detail_data
    del detail_data["url"]
    mock_response = pytest.mock_response_json(expected_example_detail_data, 400)
    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response
        with pytest.raises(exceptions.ZeroZeroAPIException) as excinfo:
            mo = Example.create(**detail_data)
        assert ar.return_value.status_code != 201


def test_model_object_save(
    Example,
    expected_example_detail_data,
    expected_options,
    expected_example_detail_url,
):
    """Tests save in the case when there is existing data"""
    mock_response = pytest.mock_response_json(expected_example_detail_data, 200)

    mo = Example(**expected_example_detail_data)
    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response
        mo.save()
        ar.assert_called_with(
            "PUT",
            expected_example_detail_url,
            data=json.dumps(expected_example_detail_data),
        )

    assert expected_example_detail_url == mo.url
    assert expected_example_detail_data == mo._data


def test_model_object_save_new(
    set_token,
    expected_example_detail_data,
    expected_options,
    expected_example_list_url,
    expected_example_detail_url,
    Example,
):
    """Tests save in the case when its new data"""
    input_data = expected_example_detail_data.copy()
    del input_data["url"]
    mock_response = pytest.mock_response_json(expected_example_detail_data, 201)

    mo = Example(**input_data)
    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response
        mo.save()
        ar.assert_called_with(
            "POST",
            expected_example_list_url,
            data=json.dumps(input_data),
        )
    assert expected_example_detail_data == mo._data
    assert expected_example_detail_data == mo._data
    assert expected_example_detail_url == mo.url


def test_model_object_save_non_200(
    set_token, expected_example_detail_data, expected_options, Example
):
    "Test that BaseModel returns a bad request when no __ is passed"
    mock_response = pytest.mock_response_json(expected_example_detail_data, 400)
    mo = Example(**expected_example_detail_data)
    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response
        with pytest.raises(exceptions.ZeroZeroAPIException) as excinfo:
            mo.save()
        assert ar.return_value.status_code != 200


def test_model_object_save_not_loaded(
    set_token,
    expected_example_detail_data,
    expected_example_detail_url,
    expected_options,
    Example,
):
    """Test that, if a field has not been evaluated and save is called, an exception is raised"""
    mock_response = pytest.mock_response_json(
        expected_example_detail_data,
        200,
    )
    example = Example(url=expected_example_detail_url)

    with pytest.raises(exceptions.ZeroZeroClientException) as excinfo:
        example.save()
    assert "can't save unloaded Model" in str(excinfo.value)


def test_model_object_update(
    set_token,
    expected_example_detail_data,
    expected_options,
    expected_example_detail_url,
    Example,
):
    """Test BaseModel updates record with PUT call and new data has been added"""
    expected_new_data = {"char": "new value"}
    mock_response = pytest.mock_response_json(expected_new_data, 200)
    mo = Example(**expected_example_detail_data)
    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response
        mo.update(expected_new_data)
        ar.assert_called_with(
            "PUT",
            expected_example_detail_url,
            data=json.dumps(expected_new_data),
        )
    assert expected_example_detail_url == mo.url
    assert expected_new_data == mo._data


def test_model_object_delete(
    Example,
    expected_example_detail_data,
    expected_options,
    expected_example_detail_url,
):
    """Test that BaseModel properly deletes with delete() call"""
    mock_response = pytest.mock_response_json(expected_example_detail_data, 200)
    mo = Example(**expected_example_detail_data)
    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response
        mo.delete()
    ar.assert_called_with("DELETE", expected_example_detail_url)
    assert expected_example_detail_url == mo.url
    assert expected_example_detail_data == mo._data


### exampleschild tests ###

# TODO: make examples_child fixture later
def test_model_object_save_with_fk(
    expected_exampleschild_detail_data,
    expected_example_detail_data,
    expected_options,
    expected_options_with_fk,
    expected_exampleschild_detail_url,
    Example,
    ExamplesChild,
):
    """Test that BaseModel saves record with a fk ref to parent"""
    mock_response = pytest.mock_response_json(
        expected_exampleschild_detail_data, 200
    )
    detail_data_with_model_object = expected_exampleschild_detail_data.copy()
    detail_data_with_model_object["parent"] = Example(
        **expected_example_detail_data
    )

    mo = ExamplesChild(**detail_data_with_model_object)
    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response
        mo.save()
        ar.assert_called_with(
            "PUT",
            expected_exampleschild_detail_url,
            data=json.dumps(expected_exampleschild_detail_data),
        )
    assert expected_exampleschild_detail_url == mo.url
    assert expected_exampleschild_detail_data == mo._data
    assert expected_exampleschild_detail_data == mo._data
    assert expected_exampleschild_detail_url == mo.url


def test_model_object_with_fk(
    build_client,
    expected_options_with_fk,
    expected_exampleschild_detail_data,
    mock_response_options,
    expected_example_list_url,
    ExamplesChild,
):
    """Test if BaseModel has dataset does not call the server"""
    ec = ExamplesChild(**expected_exampleschild_detail_data)
    assert expected_exampleschild_detail_data == ec._data
    assert expected_exampleschild_detail_data["url"] == ec.url
    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response_options
        assert isinstance(ec.parent, BaseModel)
    assert expected_exampleschild_detail_data["parent"] == ec.parent.url


def test_model_object_create_with_fk(
    Example,
    ExamplesChild,
    expected_exampleschild_detail_data,
    expected_example_detail_data,
    expected_options,
    expected_exampleschild_list_url,
    expected_options_with_fk,
    expected_exampleschild_detail_url,
):
    exampleschild_detail_data = expected_exampleschild_detail_data.copy()
    mock_response = pytest.mock_response_json(
        expected_exampleschild_detail_data, 201
    )
    del exampleschild_detail_data["url"]
    detail_data_with_model_object = exampleschild_detail_data.copy()
    detail_data_with_model_object["parent"] = Example(
        **expected_example_detail_data
    )
    with mock.patch("zerozero_pilot.model.authenticated_request") as ar:
        ar.return_value = mock_response
        mo = ExamplesChild.create(
            **detail_data_with_model_object,
        )
        ar.assert_called_with(
            "POST",
            expected_exampleschild_list_url,
            data=json.dumps(exampleschild_detail_data),
        )
    assert expected_exampleschild_detail_url == mo.url
    assert expected_exampleschild_detail_data == mo._data
