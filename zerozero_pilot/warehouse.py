import traceback
import json

from datetime import datetime, timedelta, timezone
from sqlalchemy import create_engine

from zerozero_pilot.client import get_model


def run_query_report_task(report_id, warehouse_url, models):
    QueryReport = models.zerozero.QueryReport
    QueryReportLog = models.zerozero.QueryReportLog
    report = QueryReport.get(pk=report_id)
    report_log = QueryReportLog(
        report=report.url,
        start_time=datetime.now(tz=timezone.utc).isoformat(),
        name=report.name,
    )
    report_log.save()

    try:
        _run_query_report_task(report, warehouse_url, models)
    except BaseException:
        report_log.error = traceback.format_exc()
        complete_log(report_log, success=False)
        raise

    complete_log(report_log)


def _run_query_report_task(report, warehouse_url, models):
    model_class = get_model(report.model, models)
    data_frame = model_class.csv(
        where=report.where, order=report.order, fields=report.fields
    )
    warehouse_engine = create_engine(warehouse_url)
    data_frame.to_sql(
        report.slug.replace("-", "_"), warehouse_engine, if_exists="replace"
    )


def get_ready_query_reports(models):
    QueryReport = models.zerozero.QueryReport
    QueryReportLog = models.zerozero.QueryReportLog

    report_where = {"interval__isnull": False}
    report_df = QueryReport.csv(fields=["id", "interval"], where=report_where)
    if report_df.empty:
        return []

    ids = list(report_df["id"])
    intervals = set(report_df["interval"])

    where_clauses = []
    for interval in intervals:
        where_clauses.append(
            {
                "AND": [
                    {"report__interval": interval},
                    {
                        "start_time__gt": (
                            datetime.now(tz=timezone.utc)
                            - timedelta(minutes=interval)
                        ).isoformat()
                    },
                ]
            }
        )

    log_where = (
        {"OR": where_clauses} if len(where_clauses) > 1 else where_clauses[0]
    )
    recent_logs = QueryReportLog.csv(fields=["report__id"], where=log_where)
    recent_report_ids = (
        [] if recent_logs.empty else list(set(recent_logs["report__id"]))
    )
    return [id for id in ids if not id in recent_report_ids]


def complete_log(report_log, success=True):
    report_log.end_time = datetime.now(tz=timezone.utc).isoformat()
    report_log.success = success
    report_log.save()
