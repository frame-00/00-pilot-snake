from requests import request

from zerozero_pilot import exceptions


def authenticated_request(*args, **kwargs):
    if not TOKEN:
        raise exceptions.ZeroZeroClientException("No token exception")
    headers = {
        "Authorization": "Token {}".format(TOKEN),
        "Content-Type": "application/json",
    }
    kwargs["headers"] = headers
    return request(*args, **kwargs)
