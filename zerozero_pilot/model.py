import io
import json
import os
import pandas as pd
import sys
from typing import Any
from urllib.parse import urljoin
from requests import request

from zerozero_pilot import exceptions
from zerozero_pilot.request import authenticated_request


class Model(type):
    list_url: str = None
    """The top-level url of the Model"""

    def __init__(cls, *args, **kwargs):
        """Initializes Model.metadata and Model._related_models attributes."""
        cls.metadata: dict = None
        cls._related_models = None

    def __repr__(cls):
        """I forget why we're doing this :)"""
        return cls.__name__

    def get_metadata(cls):
        """Used for accessing the options associated with the model,
        including name, actions, fields, and url.
        If metadata is not already set, places an options request to the
        list_url."""
        if not cls._metadata:
            response = authenticated_request("OPTIONS", cls.list_url)
            cls._metadata = response.json()
            fields = cls._metadata["fields"]
            for name, info in fields.items():
                related_name = info.get("related_name")
                if name != "url" and related_name:
                    app_name, model_name = related_name.split(".")
                    RelatedBaseModel = getattr(
                        getattr(models, app_name), model_name
                    )
                    info["related_model"] = RelatedBaseModel
        return cls._metadata

    def set_metadata(cls, metadata):
        """set/override metadata"""
        cls._metadata = metadata
        return cls._metadata

    metadata = property(get_metadata, set_metadata)

    @property
    def related_models(cls):
        """Getter for _related_models atteribute"""
        if cls._related_models is not None:
            return cls._related_models
        fields = cls.metadata["fields"]
        cls._related_models = {}
        for name, info in fields.items():
            related_model = info.get("related_model")
            if related_model:
                cls._related_models[name] = related_model
        return cls._related_models


class ModelEncoder(json.JSONEncoder):
    """Adds functionality to the JSONEncoder class to return the url
    of a Model object. Otherwise, behaves as a typical JSONEncoder"""

    def default(self, obj):
        if isinstance(obj, BaseModel):
            return obj.url
        return json.JSONEncoder.default(self, obj)  # pragma: no cover


def _set_property(name, info):
    """defines and returns a method? function? (set_property) that acts on a Model
    and takes in a value. But it also knows what name and info are. HALP"""

    def set_property(self, value):
        if info["read_only"]:
            raise exceptions.ZeroZeroReadOnlyException("read only")
        self._data[name] = value
        return self._data[name]

    return set_property


def _get_property(name, info):
    """Return the get_property method. Why?? Has to do with initializing a
    BaseModel object."""

    def get_property(self):
        related_model = info.get("related_model")
        if not hasattr(self._data[name], "list_url") and related_model:
            RelatedBaseModel = related_model
            self._data[name] = RelatedBaseModel(url=self._data[name])
        return self._data[name]

    return get_property


class BaseModel(metaclass=Model):
    def __new__(cls, **data):
        """Create a new Model object with getters and setters for all attributes. [??]"""
        fields = cls.metadata["fields"]
        for name, info in fields.items():
            if name != "url":  # TODO: skip on type
                get_property = _get_property(name, info)
                get_name = "get_{}".format(name)
                setattr(cls, get_name, get_property)
                set_property = _set_property(name, info)
                set_name = "set_{}".format(name)
                setattr(cls, set_name, set_property)
                setattr(
                    cls,
                    name,
                    property(
                        getattr(cls, get_name),
                        getattr(cls, set_name),
                    ),
                )
        return super().__new__(cls)

    def __init__(self, **data):
        """Initialize a Model object. Set url and data. Determine whether model is
        already loaded."""
        self.url = data.get("url", None)
        self._data = data.copy()
        self.loaded = self.url and len(self.__data.keys()) > 1

    @classmethod
    def _list(
        cls,
        page: int,
        page_size: int = None,
        where: dict = None,
        order: list = None,
    ):
        """private methods are not show by mkdocs"""
        if not page_size:
            page_size = 1000
        query = {}
        if where:
            query["where"] = where
        if order:
            query["order"] = order
        query = json.dumps(query)
        params = {"page": page, "page_size": page_size, "query": query}

        response = authenticated_request("GET", cls.list_url, params=params)
        if response.status_code != 200:
            raise exceptions.ZeroZeroAPIException(response.content)
        return response.json()

    @classmethod
    def list(
        cls, page_size: int = None, where: dict = None, order: list = None
    ):
        """Method for collecting all results belonging to the given Model via batched
        requests.

        Parameters
        ----------
        page_size: int
        The number of records to collect from each batched call.

        where: dict
            Parameter used to filter results. See the following examples:

            - Get all records where the value of the "name" fireld matches
            the name provided exactly:
                where={"name": "match-this-name"}

            * Get all records where the value of the "spend" field is
            greater than $100:
                where={"spend__gt": 100}

            * Get all records where the "id" is either 3, 5, or 8:
                where={"id__in": [3, 5, 8]}

            Conditions can be combined into a single dictionary, ie:
                where={"name": "match-this-name",
                        "spend__gt": 100,
                        "id__in": [3, 5, 8]}
            This will return only the rows that match all three conditions.

            Visit https://docs.djangoproject.com/en/4.0/ref/models/querysets/
            for more inform on possible filtering conditions.

        order: list
            Parameter used to order results. Results will be sorted in
            ascending order. Multiple fields can be passed.


        Returns
        -------
        A generator of Model instances, each representing on row of the results.
        ## DOUBLE CHECK THIS^^

        """
        next_page = True
        page = 1
        while next_page:
            resp = cls._list(
                page=page, page_size=page_size, where=where, order=order
            )
            next_page = resp["next"]
            results = resp["results"]
            page += 1
            for data in results:
                yield cls(**data)

    @classmethod
    def csv(cls, where=None, order=None, fields=None):
        """Method for collecting all results belonging to the given Model and
        returning the results as a DataFrame.
        # TODO: change the method name to "df" and update where needed.

        Parameters
        ----------
        where: dict
            Refer to list() method for more information about the "where"
            parameter.

        order: list
            Refer to list() method for more information about the "order"
            parameter.

        fields: list
            A list of all fields that should be included in the resulting
            DataFrame.

        Returns
        -------
        A pandas DataFrame of all results."""
        query = {}
        if where:
            query["where"] = where
        if order:
            query["order"] = order
        if fields:
            query["fields"] = fields

        query_json = json.dumps(query)
        params = {"query": query_json, "format": "csv"}

        response = authenticated_request("GET", cls.list_url, params=params)
        if not response._content:
            return pd.DataFrame()

        return pd.read_csv(io.StringIO(response.content.decode("utf-8")))

    @classmethod
    def get(cls, pk: Any):
        """Retrieve a single entry from the Model, based on that entry's
        primary key (internal, sequentially-assigned id)

        Parameters
        ----------
            pk: Any [QQ: should this be int?]

            The desired entry's primary key/identifier

        Returns
        ---------
            A single Model instance of the entry with the provided pk"""

        built_url = urljoin(cls.list_url, "{}/".format(pk))
        return cls(url=built_url)

    @classmethod
    def create(cls, **data):
        """Create a single entry on the Model.

        Parameters
        ----------
            data: dict

            A dictionary of Model field: value pairs.

        Returns
        ---------
            A single Model instance built with the data provided.
        """

        response = authenticated_request(
            "POST",
            cls.list_url,
            data=json.dumps(data, cls=ModelEncoder),
        )
        if response.status_code != 201:
            raise exceptions.ZeroZeroAPIException(response.content)
        data = response.json()
        return cls(**data)

    def __get_data(self):
        if not self.loaded:
            response = authenticated_request("GET", self.url)
            self._data = response.json()
        return self.__data

    def __set_data(self, data):
        self.__data = data
        self.loaded = True
        return self.__data

    _data = property(__get_data, __set_data)

    def update(self, data):
        self._data = data.copy()
        self.save()

    def save(self):
        """Method for commiting a new entry to the database."""
        if not self.url:
            self.__dict__.update(self.__class__.create(**self.__data).__dict__)
        elif not self.loaded:
            raise exceptions.ZeroZeroClientException(
                "can't save unloaded Model"
            )
        else:
            response = authenticated_request(
                "PUT",
                self.url,
                data=json.dumps(self._data, cls=ModelEncoder),
            )
            if response.status_code != 200:
                raise exceptions.ZeroZeroAPIException(response.content)
            self.__data = response.json()

    def delete(self):
        # can we remove the "data =" part?
        data = authenticated_request("DELETE", self.url)
