#!/usr/bin/env python

from setuptools import setup

setup(
    name="00-pilot",
    version="0.0.0",
    description="",
    author="Mikela Clemmons",
    author_email="glassresistor@gmail.com",
    url="",
    packages=["zerozero_pilot"],
    license_files=("LICENSE", "LICENSE.CNPLv7.md", "LICENSE.proprietary.md"),
    include_package_data=True,
    install_requires=[
        "requests~=2.27",
        "pandas~=1.4.0",
        "SQLAlchemy~=1.4.31",
    ],
    extras_require={
        "dev": [
            "tox-conda",
            "pytest~=6.2.5",
            "pytest-cov~=3.0.0",
            "pytest-randomly~=3.10.2",
            "pytest-repeat~=0.9.1",
            "click~=7.0",
            "black~=21.11b1",
        ]
    },
)
